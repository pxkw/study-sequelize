# About

Repository to study [sequelize](sequelizejs.com)

# Requirements

- Node.js
- MySQL
  - see `config/config.json`

# QuickStart

```
npm install
npm run db:migrate
npm run db:seed
npm start
```

then

```
curl -X GET http://localhost:8080/api/userinfos
curl -X GET http://localhost:8080/api/userinfos?userid=29
curl -X GET http://localhost:8080/api/userinfos/10/?userid=29
```
or

```
curl -X POST http://localhost:8080/api/users \
  -H "Accept: application/json" \
  -H "Content-type: application/json" \
  -d '{"first_name": "new first", "last_name": "new last", "bio": "new bio"}'
```
