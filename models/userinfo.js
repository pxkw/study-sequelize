'use strict';
module.exports = function(sequelize, DataTypes) {
  var UserInfo = sequelize.define('UserInfo', {
    email: DataTypes.STRING,
    user: DataTypes.INTEGER
  }, { timestamps: false });
  return UserInfo;
};
