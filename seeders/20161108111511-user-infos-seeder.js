'use strict';

var models = require('../models')

module.exports = {
  up: function (queryInterface, Sequelize) {
    return models.User.findAll().then(function (users) {
      return queryInterface.bulkInsert('UserInfos', [
        {email: 'test1@example.com', user: users[0].id},
        {email: 'test2@example.com', user: users[1].id},
        {email: 'test3@example.com', user: users[0].id}
      ]);
    });

    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('UserInfos', null, {});
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
