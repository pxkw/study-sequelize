'use strict';
var express = require('express');
var bodyParser = require('body-parser');
var models = require('./models');

models.User.hasMany(models.UserInfo, {foreignKey: 'id'});
models.UserInfo.belongsTo(models.User, {foreignKey: 'user'});

var app = express();
app.use(bodyParser.json());

function findUserInfos(where) {
  return models.UserInfo.findAll({
    where: where.userInfo,
    include: [{model: models.User, where: where.user}]
  });
}

app.get('/api/userinfos', function (req, res) {
  var where = {user: {}};
  if (req.query.userid) {
    where.user.id = req.query.userid;
  }
  return findUserInfos(where).then(function (results) {
    res.send(results);
  })
});

app.get('/api/userinfos/:id', function (req, res) {
  var where = {userInfo: {id: req.params.id}, user: {}};
  if (req.query.userid) {
    where.user.id = req.query.userid;
  }
  return findUserInfos(where).then(function (results) {
    res.send(results);
  });
});

app.post('/api/users', function (req, res) {
  var newUser = req.body
  return models.User.create(newUser).then(function (results) {
    res.send(results);
  });
});

app.listen(8080, function () {
  console.log('App started on localhost:8080');
});
